<?php 

include_once 'lib/config.php';
include_once 'lib/funciones.php';


session_start();

//$errores=array();
// si viene por post la info
if(isset($_POST['user'],$_POST['passwd'])){
	//validar
	$user=clean($_POST['user']);
	$passwd=clean($_POST['passwd']);
	if (validar($user,$passwd)) {
		$_SESSION['user']=$user;
		header('location: index.php');
	}
	//$errores[]='Usuario o pass mal';
	
	//comprobar
}


// separar presentacion
$template=$twig->loadTemplate('login.html');

echo $template->render(array('titulo'=>'Login','errores'=>$errores));
?>