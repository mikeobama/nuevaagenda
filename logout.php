<?php

include_once 'lib/config.php';
include_once 'lib/funciones.php';

session_start();

//$errores=array();
// si viene por post la info
if(isset($_POST['salir'])){
	//validar
	unset($_SESSION['user']);
	session_destroy();
	header('Location:index.php');
	//$errores[]='Usuario o pass mal';

	//comprobar
}


// separar presentacion
$template=$twig->loadTemplate('logout.html');

echo $template->render(array('titulo'=>'Login','errores'=>$errores));

?>