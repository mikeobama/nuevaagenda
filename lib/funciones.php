<?php

// funciones auxiliares
include_once 'config.php';

function clean($dato){
	
	return htmlentities(strip_tags(trim($dato)));
	
}
/*
 * Devuelve true si la pass es válida para el usuario
 * Si no, devuelve falso
 */
function validar($user,$pass){
	global $root;
	global $passwd_admin;
	if($user ==$root and $pass==$passwd_admin)
		return true;
	else 
		return false;
}
?>