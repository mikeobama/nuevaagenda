<?php

// Datos de configuración

// Datos del administrador
$root='root';
$passwd_admin='root';

// Configuracion twig
require_once realpath(dirname(__FILE__) .'/../vendor/twig/twig/lib/Twig/Autoloader.php');

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__). '/../vistas'));
$twig = new Twig_Environment($loader);

?>